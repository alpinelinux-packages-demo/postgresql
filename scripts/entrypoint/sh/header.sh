set -o errexit -o nounset -o noglob -o pipefail
(. /etc/os-release ; echo $PRETTY_NAME $VERSION_ID)
set -o xtrace
