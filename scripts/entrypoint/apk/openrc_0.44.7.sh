. $CI_PROJECT_DIR/scripts/entrypoint/sh/header.sh


if ! id -u user &> /dev/null ; then
    apk add \
            --update-cache \
            --quiet \
        openrc \
        setpriv \
        sudo \
    ;
    adduser -D user
    echo "user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/user
    openrc sysinit
    openrc boot
    openrc default
    openrc shutdown
    fi
openrc sysinit
openrc boot
openrc default
( ls /run/openrc/softlevel || true )
env \
    HOME=/home/user \
    LOGNAME=user \
    USER=user \
    SUDO=sudo \
    setpriv \
        --reuid=$(id -u user) \
        --regid=$(id -g user) \
        --init-groups \
        -- \
        "$@"
x=$?
openrc shutdown
exit $x

#^ OpenRC is called twice (config and restart).


