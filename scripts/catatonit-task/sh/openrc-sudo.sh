. $CI_PROJECT_DIR/scripts/entrypoint/sh/header.sh

openrc sysinit
openrc boot
openrc default
env \
    HOME=/home/user \
    LOGNAME=user \
    USER=user \
    SUDO=sudo \
    setpriv \
        --reuid=$(id -u user) \
        --regid=$(id -g user) \
        --init-groups \
        -- \
        "$@"
x=$?
openrc shutdown
exit $x

#^ Trap is not working (from gitlab-ci.yml), maybe it would wor^ with bash.
#^ ["/bin/sh", "-c", "trap 'openrc shutdown' 0 ; openrc ; su user \\\"\\\$@\\\"", "--"]
#^ SUDO=sudo exec time su user \\"\\$@\\" ; x=$?
